from __future__ import annotations

from typing import TYPE_CHECKING

from nio import AsyncClient, MatrixRoom, Event, RoomMessageText

if TYPE_CHECKING:
    from core.plugin_manager import PluginManager
    from core.config import AppConfig
    from core.storage import Storage


class Bundle:
    def __init__(
            self,
            client: AsyncClient,
            store: Storage,
            app_config: AppConfig,
            plugin_manager: PluginManager
    ):
        self.client = client
        self.store = store
        self.app_config = app_config
        self.plugin_manager = plugin_manager



class Message(Bundle):
    def __init__(
        self,
        client: AsyncClient,
        store: Storage,
        app_config: AppConfig,
        raw_msg: str,
        room: MatrixRoom,
        event: RoomMessageText,
        plugin_manager: PluginManager
    ):
        super().__init__(client, store, app_config, plugin_manager)
        self.raw_msg = raw_msg
        self.room = room
        self.event = event
        self.args= raw_msg.split()


class Command(Message):
    def __init__(
        self,
        client: AsyncClient,
        store: Storage,
        config: AppConfig,
        raw_msg: str,
        room: MatrixRoom,
        event: RoomMessageText,
        plugin_manager: PluginManager
    ):
        """Responder for handling user commands.
        Args:
            client: The client to communicate to matrix with.
            store: Bot storage.
            config: Bot configuration parameters.
            raw_msg: plaintext message of the event that triggered this interaction
            command: User's command and arguments, minus the command prefix.
            room: The room the command was sent in.
            event: Message event.
        """
        super().__init__(client, store, config, raw_msg, room, event, plugin_manager)
        self.command = raw_msg.split(' ')[0]
        self.args = raw_msg.split()[1:]
