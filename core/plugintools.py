#!/usr/bin/env python3
from __future__ import annotations

from typing import List, TYPE_CHECKING
import logging

from nio import AsyncClient, MatrixRoom, Event

from core.chat_functions import react_to_event, send_text_to_room, send_file_to_room
from core.hook import CommandHook, TriggerHook, TimerHook
from core.storage import Storage
from core.config import AppConfig, PluginConfig

if TYPE_CHECKING:
    from core.plugin_manager import PluginManager, _Plugin


logger = logging.getLogger(__name__)

class Responder:
    def __init__(
            self,
            client: AsyncClient,
            room: MatrixRoom,
            event: Event,
            args: List[str]
    ):
        '''Helper object for plugins. Use this to respond to messages or events.
        Args:
            client: matrix async client
            room: matrix room
            event: event that triggered this interaction
        '''
        self.event = event
        self.args = args

        self.__client = client
        self.room = room


    async def send_text(self, text: str, reply=False, notice=True) -> None:
        if reply and self.event != None:
            await send_text_to_room(
                self.__client,
                self.room.room_id,
                text,
                reply_to_event_id=self.event.event_id,
                notice=notice
            )
        else:
            await send_text_to_room(self.__client, self.room.room_id, text)


    async def send_file(self, filepath: str, reply=False) -> None:
        if reply and self.event != None:
            await send_file_to_room(
                self.__client,
                self.room.room_id,
                filepath,
                reply_to_event_id=self.event.event_id
            )
        else:
            await send_file_to_room(self.__client, self.room.room_id, filepath)


    def sender_power_level(self) -> int:
        '''Get message sender's power level
        Returns:
            Power level of the sender
        '''
        return self.room.power_levels.get_user_level(self.event.sender)


    async def react_to_event(self, reaction):
        await react_to_event(
            self.__client, self.room.room_id, self.event.event_id, reaction
        )


class Sender:
    def __init__(
            self,
            client: AsyncClient
    ):
        '''Helper object for plugins. Use this to send messages unprompted.
        Args:
            client: matrix async client
        '''
        self.__client = client
        self.__room : MatrixRoom


    def set_room_id(self, room_id: str) -> None:
        '''Set room id
        Args:
            room_id: ID of the room to send to
        '''
        self.__room = MatrixRoom(room_id, self.__client.user_id)


    async def send_text(self, text: str, notice=True) -> None:
        await send_text_to_room(
            self.__client,
            self.__room.room_id,
            text,
            notice=notice
        )


    async def send_file(self, img_path: str) -> None:
        await send_file_to_room(self.__client, self.__room.room_id, img_path)



class Bot:
    def __init__(
            self,
            client: AsyncClient,
            store: Storage,
            cache: dict,
            app_config: AppConfig,
            plugin_manager: PluginManager,
    ):
        '''Helper object for plugins.
        Use this to interact with storage, application config, and plugin manager.
        Args:
            store: database storage for the bot
            config: application config object
            plugin_manager: plugin manager object
        '''
        self.client = client
        self.store = store
        self.plugin_cache = cache
        self.app_config = app_config
        self.plugin_manager = plugin_manager


    async def quit(self):
        '''Close the client connection
        '''
        logger.debug('Closing client connection and quitting')
        await self.client.close()
        exit(0)



def command(*command_names, params: str):
    '''Decorator for commands
    Args:
        *command_names: Arbitrary number of aliases to associate this command with
        desc: Description of this command
    '''
    def _decorator(func):
        # attach a hook to the decorated function
        func.hook = CommandHook(func, command_names, params)
        return func
    return _decorator


def trigger(*triggers, description: str):
    '''Decorator for triggers
    Args:
        *triggers: Arbitrary number of triggers to use
        desc: Description of this trigger
    '''
    def _decorator(func):
        func.hook = TriggerHook(func, triggers, description)
        return func
    return _decorator


def timer(description: str, hours: int = 0, minutes: int = 0, repeat=False):
    '''Decorator for timed triggers.
    Execute something after waiting a certain amount of time.
    Args:
        hours: wait for this many hours
        minutes: wait for this many minutes
        repeat: do it again periodically
    '''
    def _decorator(func):
        func.hook = TimerHook(func, hours, minutes, repeat, description)
        return func
    return _decorator

# export this to the importing plugin
PluginConfig = PluginConfig
