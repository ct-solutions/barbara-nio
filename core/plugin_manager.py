#!/usr/bin/env python3

from __future__ import annotations

import glob
import importlib
from os.path import basename, isdir
from os import getcwd
import logging
from inspect import getfullargspec
from datetime import datetime

from typing import Any, Dict, List, TYPE_CHECKING, Union, Optional

from core.config import PluginConfig
from core.hook import Hook, CommandHook, TriggerHook
from core.plugintools import Responder, Sender, Bot

if TYPE_CHECKING:
    from types import ModuleType
    from core.config import AppConfig
    from core.message import Bundle, Message, Command

logger = logging.getLogger(__name__)


class PluginManager:
    def __init__(self, config: AppConfig):
        '''Loads plugins and provides endpoints for running plugin hooks.
        Contains multiple plugins.
        Args:
            config: AppConfig instance of the bot.
        '''
        # {'plugin name': _Plugin, ...}
        self.__plugins: Dict[str, _Plugin] = {}

        # {'command name': _Plugin, ...}
        self.__commands: Dict[str, _Plugin] = {}

        # {'trigger string': [_Plugin, ...], ...}
        self.__triggers: Dict[str, List[_Plugin]] = {}

        # [_Plugin, _Plugin, ...]
        self.__timers: List[_Plugin] = []
        self.__config = config

        self.__load_all_plugins()


    def get_all_plugins(self) -> List[_Plugin]:
        '''Get a list of all loaded plugins.
        Returns:
            A list of _Plugin objects.
        '''
        return list(self.__plugins.values())


    def get_trigger_hooks(self, trigger: str) -> Union[list[TriggerHook], None]:
        '''Get a list of trigger hooks that match the given trigger
        Args:
            trigger: trigger string
        Returns:
            A list of hooks with the matching trigger
        '''
        # don't bother if trigger doesn't exist
        if trigger not in self.__triggers:
            return

        result: list[TriggerHook] = []
        plugins: list[_Plugin] = self.__triggers[trigger]
        for plug in plugins:
            hooks = plug.get_hooks('trigger')
            for hook in hooks:
                result.append(hook)
        return result


    def get_timer_hooks(self, timer_func_name: str):
        '''Get a list of timer hooks whose function matches a certain name
        Args:
            timer_func_name: name of the function to look for
        Returns:
            A list of hooks with the matching name
        '''
        # don't bother if there are no timers
        if not self.__timers:
            return None

        result = []
        plugins = self.__timers
        for plug in plugins:
            hooks = plug.get_hooks('timer')
            for hook in hooks:
                if hook.func.__name__ == timer_func_name:
                    result.append(hook)
        return result


    def get_command_hook(self, command: str) -> Union[CommandHook, None]:
        '''Given command, get the corresponding hook
        Args:
            command: command, such as 'help' or 'echo'
        Returns:
            CommandHook associated with the command, or None if not found
        '''
        # don't bother if command doesn't exist
        if command not in self.__commands:
            return None

        plugin = self.__commands[command]
        hooks = plugin.get_hooks('command')
        for hook in hooks:
            if hook.match(command):
                return hook

        # this should never happen if all plugins are loaded properly
        logger.warn("Failed to get command hook from '{plugin.name}', maybe improperly loaded plugin?")
        return None


    def load_plugin(self, name: str) -> bool:
        '''Load a plugin
        Params:
            name: name of the plugin
        Returns:
            True if plugin is found in the filesystem and loaded successfully,
            False otherwise
        '''
        # don't bother if it already exists
        if name in self.__plugins:
            return False

        loaded = self.__load_plugin(name)
        return loaded


    async def run_command_hook(self, bundle: Command) -> None:
        '''Given input, find and execute a command hook that has a matching
        command trigger.
        Args:
            bundle: a command bundle object containing parameters
        '''
        command = bundle.command
        if command not in self.__commands:
            logger.debug(f"Command '{command}' not found -- skipping")
            return
        try:
            await self.__commands[command].invoke_command(bundle)
        except Exception as e: 
            logger.warn(f"Failed to run command '{command}': {e}")


    async def run_timer_hooks(self, bundle: Bundle):
        '''Given input, execute all timer hooks
        Args:
            bundle: a bundle object containing parameters
        '''
        for plugin in self.__timers:
            logger.debug(f"Running timers for plugin '{plugin.name}'")
            try:
                await plugin.invoke_timers(bundle)
            except Exception as e:
                logger.warn(f"Failed to run timer: {e}")


    async def run_trigger_hooks(self, bundle: Message) -> None:
        '''Given input, find and execute trigger hooks with the matching trigger
        Args:
            bundle: a message bundle object containing parameters
        '''
        for plugins in self.__triggers.values():
            for plugin in plugins:
                try:
                    await plugin.invoke_triggers(bundle)
                except Exception as e:
                    logger.warn(f"Failed to run trigger: {e}")


    def unload_plugin(self, name: str) -> bool:
        '''Unload a plugin
        Args:
            name: name of the plugin to unload
        Returns:
            True if named plugin was found and unloaded successfully,
            False otherwise
        '''
        plugins = self.__plugins
        plugin_to_remove = plugins.get(name)

        # plugin not found
        if not plugin_to_remove:
            return False

        # pop plugin from plugins list, hopefully
        for plug in plugins:
            if plugins[plug].name == name:
                plugins.pop(plug)
                break

        # same for command:plugin mapping
        commands = self.__commands
        remove = []
        for cmd in commands:
            if commands[cmd].name == name:
                remove.append(cmd)
        for cmd in remove:
            commands.pop(cmd)


        # same for the list of triggers
        triggers = self.__triggers
        for trig in triggers:
            if plugin_to_remove in triggers[trig]:
                triggers[trig].remove(plugin_to_remove)


        # same for timers
        self.__timers.remove(plugin_to_remove)

        # get rid of the plugin
        del plugin_to_remove
        return True


    def reload_plugin(self, name: str) -> bool:
        '''Reload a plugin
        Args:
            name: name of the plugin to reload
        Returns:
            True if named plugin was found and reloaded successfully,
            False otherwise
        '''
        unloaded = self.unload_plugin(name)
        if not unloaded:
            return False

        # now load it again
        self.__load_plugin(name)

        return True


    def __get_plugin_folders(self) -> List[str]:
        '''Get a list of plugin folder names
        Assumes that plugin folders exist in <project root>/plugins/*
        Only the basenames are fetched, e.g. <project root>/plugins/myplugin -> myplugin
        Returns:
            List of plugin folder names
        '''
        plugin_folders = [
            basename(thing) for thing in glob.glob('plugins/*')
            if isdir(thing) and not thing.endswith('__pycache__')
        ]
        plugin_folders.sort()
        return plugin_folders


    def __load_all_plugins(self) -> None:
        '''Load all plugins from all detected plugin folders
        '''
        logger.debug(f"Loading all plugins | Working directory: {getcwd()}")
        plugin_folders = self.__get_plugin_folders()
        logger.info(f"{len(plugin_folders)} potential plugins detected")
        for folder in plugin_folders:
            if folder not in self.__config.disable_plugins:
                self.__load_plugin(folder)


    def __load_plugin(self, name: str) -> bool:
        '''Import and load a plugin.
        Assume plugins are in <project root>/plugins/<plugin name>/<plugin name>.py
        Args:
            name: basename of the plugin folder, e.g. "myplugin"
        '''
        try:
            logger.info(f"Loading plugin: '{name}")
            imported_plugin = importlib.import_module(f'plugins.{name}.{name}')
        except Exception as e:
            logger.info(f"Failed to load plugin '{name}': {e}")
            return False

        plugin = _Plugin(name, imported_plugin)
        self.__plugins[name] = plugin
        self.__map_command_to_plugin(plugin)
        self.__map_trigger_to_plugin(plugin)
        self.__load_timer_plugins(plugin)

        return True


    def __map_command_to_plugin(self, plugin: _Plugin):
        for hook in plugin.get_hooks('command'):
            for command in hook.commands:
                if command in self.__commands:
                    logger.warning(
                        f"{plugin.name}: '{command}' is already registered, skipping"
                    )
                else:
                    self.__commands[command] = plugin


    def __map_trigger_to_plugin(self, plugin: _Plugin):
        for hook in plugin.get_hooks('trigger'):
            for trigger in hook.triggers:
                if trigger not in self.__triggers:
                    self.__triggers[trigger] = []
                self.__triggers[trigger].append(plugin)


    def __load_timer_plugins(self, plugin: _Plugin):
        self.__timers.append(plugin)




class _Plugin:
    def __init__(self, plugin_name: str, module: ModuleType):
        '''Encapsulates a plugin. Contains multiple hooks.
        Args:
            plugin_name: Name of the plugin.
            module: Plugin code imported via importlib.import_module(..).
        '''
        self.name = plugin_name
        self.description = 'Default description'
        self.config = PluginConfig('./plugins/{}/config.yaml'.format(plugin_name))
        self.cache = {}

        self.__hooks: Dict[str, List[Hook]] = {}  # { hook_type : some_hook, ... }
        self.module = module

        self.__load_all_hooks()
        self.__set_plugin_description()


    def get_plugin_description(self) -> str:
        '''Get the description string of this plugin.
        Returns:
            A str object containing plugin description.
        '''
        return self.description


    def get_hooks(self, hook_type: Optional[str] = None) -> List[Any]:
        '''Get a specific type of hooks that belong to this plugin.
        Args:
            hook_type: Type of hooks to look for.
                If none, get all hooks regardless of type.
        Returns:
            A list of hooks matching hook_type, or
            all hooks if hook_type is None, or
            empty list if no hooks match hook_type
        '''
        if not hook_type:
            return [hook for value in self.__hooks.values() for hook in value]
        logger.debug(f"Fetching '{hook_type}' hooks from plugin '{self.name}'")
        if hook_type in self.__hooks:
            return [hook for hook in self.__hooks[hook_type]]
        return []

    async def invoke_command(self, bundle: Command):
        command = bundle.command
        command_hooks = self.get_hooks('command')
        for hook in command_hooks:
            if hook.match(command):
                args = generate_hook_args(bundle, hook, self)
                await hook.func(*args)
                return


    async def invoke_triggers(self, bundle: Message):
        text = bundle.raw_msg
        trigger_hooks = self.get_hooks('trigger')
        for hook in trigger_hooks:
            if hook.match(text):
                args = generate_hook_args(bundle, hook, self)
                await hook.func(*args)


    async def invoke_timers(self, bundle: Bundle):
        timer_hooks = self.get_hooks('timer')
        for hook in timer_hooks:
            if hook.match(datetime.now()):
                args = generate_hook_args(bundle, hook, self)
                await hook.func(*args)



    def __load_hook(self, hook: Hook) -> None:
        '''Categorize hook and keep a reference to it internally.
        References are kept as dict: { hook_type: [hook1, hook2, ...], ... }
        Args:
            hook: An instance of one of Hook's derived classes.
        '''
        hook_type = hook.hook_type
        logger.debug(f"Plugin '{self.name}': Loading a '{hook_type}' hook")
        if hook_type not in self.__hooks:
            self.__hooks[hook_type] = []

        self.__hooks[hook_type].append(hook)
        logger.debug(f"Plugin '{self.name}': Hook loaded: {self.__hooks[hook_type][-1]}")


    def __set_plugin_description(self) -> None:
        '''Set the description string of this plugin.
        Args:
        '''
        attributes = self.module.__dict__
        if 'plugin_description' in attributes:
            self.description = self.module.plugin_description


    def __load_all_hooks(self) -> None:
        '''Load all hooks from the plugin code.
        Args:
            module: Plugin code loaded via importlib.import_module(...).
        '''
        attributes = self.module.__dict__.values()
        for obj in attributes:
            # if it's a callable thing, then it should have a `hook` property
            if hasattr(obj, 'hook') and isinstance(obj.hook, Hook):
                self.__load_hook(obj.hook)

        logger.debug(f"Plugin '{self.name}': {len([x for row in self.__hooks.values() for x in row])} hooks loaded")


def generate_hook_args(
        bundle: Union[Bundle, Message, Command],
        hook: Hook,
        plugin: _Plugin
    ):
    '''Generate args to be sent to the hook function
    '''
    spec = getfullargspec(hook.func)
    result = []
    for thing in spec.annotations.values():
        if (thing == 'Responder' or thing == Responder):
            responder = Responder(bundle.client, bundle.room, bundle.event, bundle.args)
            result.append(responder)
        elif thing == 'Sender' or thing == Sender:
            sender = Sender(bundle.client)
            result.append(sender)
        elif thing == 'Bot' or thing == Bot:
            bot = Bot(bundle.client, bundle.store, plugin.cache, bundle.app_config, bundle.plugin_manager)
            result.append(bot)
        elif thing == 'PluginConfig' or thing == PluginConfig:
            result.append(plugin.config)
        else:
            logger.warn(f"Unrecognized argument type: {thing}")
    logger.debug(f"Generated arguments: {result}")
    return result


