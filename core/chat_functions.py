import logging
from nio.api import ResizingMethod
import requests
import magic
from PIL import Image
from typing import Optional, Union
import aiofiles.os
import os, io

from markdown import markdown
from datetime import datetime, timedelta
from nio import (
    AsyncClient,
    ErrorResponse,
    MatrixRoom,
    MegolmEvent,
    Response,
    UploadResponse,
    RoomSendResponse,
    SendRetryError,
)

logger = logging.getLogger(__name__)


async def send_text_to_room(
    client: AsyncClient,
    room_id: str,
    message: str,
    notice: bool = True,
    markdown_convert: bool = True,
    reply_to_event_id: Optional[str] = None,
) -> Union[RoomSendResponse, ErrorResponse]:
    """Send text to a matrix room.

    Args:
        client: The client to communicate to matrix with.

        room_id: The ID of the room to send the message to.

        message: The message content.

        notice: Whether the message should be sent with an "m.notice" message type
            (will not ping users).

        markdown_convert: Whether to convert the message content to markdown.
            Defaults to true.

        reply_to_event_id: Whether this message is a reply to another event. The event
            ID this is message is a reply to.

    Returns:
        A RoomSendResponse if the request was successful, else an ErrorResponse.
    """
    # Determine whether to ping room members or not
    msgtype = "m.notice" if notice else "m.text"

    content = {
        "msgtype": msgtype,
        "format": "org.matrix.custom.html",
        "body": message,
    }

    if markdown_convert:
        content["formatted_body"] = markdown(message)

    if reply_to_event_id:
        content["m.relates_to"] = {"m.in_reply_to": {"event_id": reply_to_event_id}}

    try:
        logger.debug(f"Sending message to {room_id}")
        return await client.room_send(
            room_id,
            "m.room.message",
            content,
            ignore_unverified_devices=True,
        )
    except SendRetryError:
        logger.exception(f"Unable to send message response to {room_id}")


# https://matrix-nio.readthedocs.io/en/latest/examples.html
async def _send_image_to_room(
        client: AsyncClient,
        room_id: str,
        imgpath: str,
        mime_type: str,
        content: dict,
):
    """Send image to toom.
    Args:
        client : AsyncClient
        room_id : str
        imgpath : str, file name of image
        mime_type: str, mimetype of file
        content: dict, content to send


    This is a working example for a JPG image.
        "content": {
            "body": "someimage.jpg",
            "info": {
                "size": 5420,
                "mimetype": "image/jpeg",
                "thumbnail_info": {
                    "w": 100,
                    "h": 100,
                    "mimetype": "image/jpeg",
                    "size": 2106
                },
                "w": 100,
                "h": 100,
                "thumbnail_url": "mxc://example.com/SomeStrangeThumbnailUriKey"
            },
            "msgtype": "m.image",
            "url": "mxc://example.com/SomeStrangeUriKey"
        }

    """
    if not mime_type.startswith("image/"):
        logger.warn("Dropping image message -- file does not have an image mime type.")
        return

    # god dammit
    im = Image.open(imgpath)
    (width, height) = im.size

    ## why the fuck is this returning "invalid content type: application/json"??
    #uri = content.get('url')
    #logger.debug(f"uri: {uri}")
    #server = uri.split('/')[-2]
    #media = uri.split('/')[-1]
    #logger.debug(f"requesting '{server}' for thumbnail data on '{media}'")
    #r = await nio.thumbnail(server, media, 400, 400, method=ResizingMethod.scale)
    #logger.debug(f"received: {r}")

    content['info']['w'] = width
    content['info']['h'] = height
    #content['info']['thumbnail_info'] = None
    #content['info']['thumbnail_url'] = None

    content['msgtype'] = 'm.image'

    try:
        await client.room_send(room_id, message_type="m.room.message", content=content)
        logger.debug("Image was sent successfully")
    except Exception:
        logger.warn(f"Image failed to send: {imgpath}")


async def _upload_file(
        client: AsyncClient,
        filepath: str,
):
    mime_type = magic.from_file(filepath, mime=True)
    file_stat = await aiofiles.os.stat(filepath)

    async with aiofiles.open(filepath, 'r+b') as f:
        resp, decryption_keys = await client.upload(
            f,
            content_type=mime_type,
            filename=os.path.basename(filepath),
            filesize=file_stat.st_size,
            encrypt=True
        )
    if isinstance(resp, UploadResponse):
        logger.debug("Image was uploaded successfully to server. ")
    else:
        logger.warn(f"Failed to upload image. Failure response: {resp}")
        return None, None

    content = {
        "body": os.path.basename(filepath),  # descriptive title
        "info": {
            "size": file_stat.st_size,
            "mimetype": mime_type,
        },
        "url": resp.content_uri,
        'file': {
            'url': resp.content_uri,
            'key': decryption_keys['key'],
            'iv': decryption_keys['iv'],
            'hashes': decryption_keys['hashes'],
            'v': decryption_keys['v']
        },
    }

    return mime_type, content


async def send_file_to_room(
        client: AsyncClient,
        room_id: str,
        filepath: str,
        reply_to_event_id: Optional[str] = None
):
    mime_type, content = await _upload_file(client, filepath)
    if not mime_type or not content:
        return

    # set reply, if true
    content["m.relates_to"] = {"m.in_reply_to": {"event_id": reply_to_event_id}}

    # let send_image_to_room() handle it if this is an image
    if mime_type.startswith('image/'):
        return await _send_image_to_room(
            client,
            room_id,
            filepath,
            mime_type,
            content,
        )

    if mime_type.startswith('audio/'):
        msg_type = 'm.audio'
    elif mime_type.startswith('video/'):
        msg_type = 'm.video'
    else:
        msg_type = 'm.file'

    # set message type
    content['msgtype'] = msg_type

    try:
        await client.room_send(room_id, message_type="m.room.message", content=content)
        logger.debug("File was sent successfully")
    except Exception:
        logger.warn(f"Failed to send file: {filepath}")



def make_pill(user_id: str, displayname: Optional[str] = None) -> str:
    """Convert a user ID (and optionally a display name) to a formatted user 'pill'

    Args:
        user_id: The MXID of the user.

        displayname: An optional displayname. Clients like Element will figure out the
            correct display name no matter what, but other clients may not. If not
            provided, the MXID will be used instead.

    Returns:
        The formatted user pill.
    """
    if not displayname:
        # Use the user ID as the displayname if not provided
        displayname = user_id

    return f'<a href="https://matrix.to/#/{user_id}">{displayname}</a>'


async def react_to_event(
    client: AsyncClient,
    room_id: str,
    event_id: str,
    reaction_text: str,
) -> Union[Response, ErrorResponse]:
    """Reacts to a given event in a room with the given reaction text

    Args:
        client: The client to communicate to matrix with.

        room_id: The ID of the room to send the message to.

        event_id: The ID of the event to react to.

        reaction_text: The string to react with. Can also be (one or more) emoji characters.

    Returns:
        A nio.Response or nio.ErrorResponse if an error occurred.

    Raises:
        SendRetryError: If the reaction was unable to be sent.
    """
    content = {
        "m.relates_to": {
            "rel_type": "m.annotation",
            "event_id": event_id,
            "key": reaction_text,
        }
    }

    return await client.room_send(
        room_id,
        "m.reaction",
        content,
        ignore_unverified_devices=True,
    )


async def decryption_failure(self, room: MatrixRoom, event: MegolmEvent) -> None:
    """Callback for when an event fails to decrypt. Inform the user"""
    logger.error(
        f"Failed to decrypt event '{event.event_id}' in room '{room.room_id}'!"
        f"\n\n"
        f"Tip: try using a different device ID in your config file and restart."
        f"\n\n"
        f"If all else fails, delete your store directory and let the bot recreate "
        f"it (your reminders will NOT be deleted, but the bot may respond to existing "
        f"commands a second time)."
    )

    user_msg = (
        "Unable to decrypt this message. "
        "Check whether you've chosen to only encrypt to trusted devices."
    )

    await send_text_to_room(
        self.client,
        room.room_id,
        user_msg,
        reply_to_event_id=event.event_id,
    )


def timestamp_now() -> int:
    '''Returns Unix epoch of current time in milliseconds.

    Returns:
        An integer timestamp of current time in milliseconds.
    '''
    timestamp = int(datetime.now().timestamp())
    return timestamp * 1000


def is_recent(timestamp: int, seconds: int = 30) -> bool:
    '''Determine if time represented by timestamp is "recent" or not.

    Args:
        timestamp: Unix epoch to check.
        seconds: Threshold for recency.
            Timestamp is "recent" if it happened this many seconds ago or less.
            Defaults to 30.
    '''
    delta = timestamp_now() - timestamp
    # server timestamp is in milliseconds
    threshold = timedelta(seconds=seconds).total_seconds() * 1000

    if delta < threshold:
        return True
    else:
        logger.debug("Event is too old, skipping")
        return False
