#!/usr/bin/env python3

from datetime import datetime, timedelta
from typing import Callable, Tuple, Union
from abc import ABCMeta, abstractmethod
import string

from core.chat_functions import timestamp_now

class Hook(metaclass=ABCMeta):
    @abstractmethod
    def __init__(
            self,
            func: Callable,
            hook_type: str
    ):
        '''Base class for different types of hooks.
        Args:
            func: Function to call when this hook is invoked
            triggers: Trigger words for invoking for this hook
            desc: Description of what the hook function does
            hook_type: Type of hook ('command', 'trigger', etc)
        '''
        self.func = func
        self.hook_type = hook_type

    @abstractmethod
    def match(self, arg: object) -> bool:
        '''Determine if text matches one of the triggers. Case insensitive.
        Args:
            text: Look for this word/phrase in the triggers associated with this hook
        Returns:
            True if text matches one of the triggers
            False otherwise
        '''
        pass



class CommandHook(Hook):
    def __init__(
            self,
            func: Callable,
            commands: tuple[str],
            params: str
    ):
        '''Hook for user commands.
        Args:
            func: Function to call when the user invokes this command
            commands: Command aliases for this command (help, h, cowsay, etc)
            params: syntax help string
        '''
        super().__init__(func, 'command')
        self.commands = commands
        self.params = params


    def match(self, command: str) -> bool:
        '''Returns:
            True if this hook is associated with command
            False otherwise
        '''
        return command in self.commands


class TriggerHook(Hook):
    def __init__(
            self,
            func: Callable,
            triggers: tuple[str],
            description: str
    ):
        '''Hook for trigger words.
        Args:
            func: Fucntion to call
            triggers: Trigger words/phrases for this hook
        '''
        super().__init__(func, 'trigger')
        self.triggers = triggers
        self.description = description


    def match(self, text: str):
        '''Returns:
            True if text matches one of the triggers
            False otherwise
        '''
        cleaned_text = text.translate(str.maketrans('', '', string.punctuation))
        return cleaned_text in self.triggers


class TimerHook(Hook):
    def __init__(
            self,
            func: Callable,
            hours: int,
            minutes: int,
            repeat: bool,
            description: str
    ):
        self.interval = timedelta(hours=hours, minutes=minutes)
        super().__init__(func, 'timer')
        self.repeat = repeat
        self.last_run: datetime = datetime.now() - timedelta(minutes=1)
        self.description = description


    def match(self, now: datetime) -> bool:
        '''Args:
            now: datetime object, preferably of current time
        Returns:
            True if this trigger is due to run, false otherwise
        '''
        delta = now - self.last_run
        if delta > self.interval:
            self.last_run = now
            return True
        return False
