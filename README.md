# barbara-nio 

![](img/help.png)

A Matrix bot based on [nio-template](https://github.com/anoadragon453/nio-template). 
Features derived from [nio-smith](https://github.com/alturiak/nio-smith) and [CloudBot](https://github.com/CloudBotIRC/CloudBot). Supports plugins. 


## Deploying 

Deployment options are totally not finalized and *I am so sorry!* 
I'm still working on this and I'll provide a way to cleanly deploy this in the future.

### Without Docker 

Refer to nio-template's [native installation](https://github.com/anoadragon453/nio-template/blob/master/SETUP.md#native-installation) guide. 
It should be more or less the same. 

### With Docker 

First, create the data and store directory. 
`data` is used for storing the bot config file and SQLite database file. 
`.store` is for nio's cryptographic stuff:

``` sh
mkdir -p data/.store 
```

Then, create a docker volume pointing to that directory: 

``` sh 
docker volume create \
  --opt type=none \
  --opt o=bind \
  --opt device="/path/to/data/dir" data_volume
```

Copy over `sample.config.yaml` as `config.yaml` under `data` directory: 

``` sh
cp sample.config.yaml data/config.yaml
```

Edit the config file to suit your needs. 

Configure plugins by copying each plugin's sample config file to the appropriate plugin directory. 
Sample plugin config files can be found under `plugins/PLUGIN_NAME/sample.config.yaml`. 

For example, configuring [derpi](plugins/derpi) plugin can be done via: 

``` sh
cd plugins/derpi
cp sample.config.yaml ./config.yaml 
```

And editing the appropriate fields. 

Once everything is set up, build it and let it run: 

``` sh
cd /path/to/project/root/docker 
docker-compose up --build local-checkout-dev   # sorry... 
```

For details on deploying using docker-compose, refer to nio-template's [docker-compose](https://github.com/anoadragon453/nio-template/blob/master/SETUP.md#using-docker-compose) guide. Please note that Barbara-nio differs slightly from nio-template's setup: 

1. Cryptographic store directory is moved to `data/.store`
2. Docker image is based on `debian:stable-slim` instead of Alpine. 
3. It's going to take a while to build (sorry! I was trying so many options to get this running and the Dockerfile isn't cleaned up yet)

## Plugins 

All plugins are stored in `plugins` folder.
Please read [plugins/README.md](plugins/README.md) for info on how to write new plugins. 
An example plugin is provided under [plugins/example_plugin](plugins/example_plugin) for reference. 
