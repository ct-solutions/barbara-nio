#!/usr/bin/env python3
import requests
import random
import uuid
import aiofiles
import logging
import os
from bs4 import BeautifulSoup

from core.plugintools import command, PluginConfig, Responder, Bot

plugin_description = "Fetch images from gelbooru"

logger = logging.getLogger(__name__)

@command('gel', 'gelbooru', params='<tag, tag, ... | <"--previous" | "-p">>')
async def gelbooru(responder: Responder, config: PluginConfig, bot: Bot):
    args = responder.args

    # no args, no service
    if not args:
        return

    if ('--previous' in args or '-p' in args) and bot.plugin_cache.get('previous'):
        await responder.send_text(f"{bot.plugin_cache.get('previous')}", reply=True)
        return

    api_url = 'https://gelbooru.com/index.php?page=dapi&s=post&q=index&tags='

    # get default tags for this room
    room_settings = config.get('room_settings')
    default_tags = ''
    if room_settings:
        for setting in room_settings:
            if setting.get('room_id') == responder.room.room_id:
                default_tags = setting.get('default_tags')
                break
    logger.debug(f"Default tags for this room: {default_tags}")

    filesize_limit = config.get('filesize_limit') * 1000000 if config.get('filesize_limit') else None

    # query the api and fetch all image posts
    query = f"{api_url} {default_tags} {' '.join(args)}"
    logger.debug(f"Sending query: {query}")
    data = await _get_data(query)
    if not data:
        return

    logger.debug(f"Turning data into soup... {type(data)}")
    try:
        bs_data = BeautifulSoup(data, 'xml')
    except Exception as e:
        logger.debug(f"failed to turn data into soup: {e}")
        return
    images = bs_data.find_all('post')

    # none found? stop
    if not images:
        await responder.send_text("No results found! Wrong tags or API is throttled", reply=True)
        return

    # get a random image post
    img = random.choice(images)

    # extract image URL
    link_xml = img.find('file_url')
    if not link_xml:
        await responder.send_text("Failed to fetch image URL! Sorry", reply=True)
        return
    img_url = link_xml.getText()
    if not img_url:
        return

    # generate filename
    ext = img_url.split('.')[-1]
    filename = f"{uuid.uuid4().hex}.{ext}"

    # get the image bytes
    try:
        data = await _get_data(img_url)
        if not data:
            return
    except:
        logger.debug(f"failed to get image: {img_url}")
        return

    # use the sample link if this image is too big
    if filesize_limit and len(data) > filesize_limit:
        logger.debug("file is too big, fetching sample_url instead")
        link_xml = img.find('sample_url')
        if not link_xml:
            return
        img_url = link_xml.getText()
        if not img_url:
            return

        data = await _get_data(img_url)
        if not data:
            return

    # finally, save it
    async with aiofiles.open(filename, 'wb') as f:
        logger.debug(f"Writing to file {filename}")
        await f.write(data)

    # cache the page link
    cache = bot.plugin_cache
    img_id = img.find('id').getText()
    page_url = f"https://gelbooru.com/index.php?page=post&s=view&id={img_id}"
    cache['previous'] = page_url

    # send it
    await responder.send_file(filename, reply=True)

    try:
        logger.debug(f"Deleting file {filename}")
        os.remove(filename)
    except:
        logger.warn(f"failed to delete temp file: {filename}")


async def _get_data(url, text=False):
    r = requests.get(url)
    if r.status_code != 200:
        return
    if text:
        return r.text
    return r.content
