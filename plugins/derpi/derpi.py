#!/usr/bin/env python3

import aiofiles
import requests
import json
import random
import uuid
from os.path import splitext
import os

from typing import Union, Optional
import logging

from core.plugintools import Responder, Bot, PluginConfig, command

plugin_description = "Derpibooru image search plugin"

logger = logging.getLogger(__name__)
API = 'https://derpibooru.org/api/v1/json'

@command('derpi', 'd', params='''<tag, tag, ... | <"--prevous" | "-p">>''')
async def derpi(responder: Responder, bot: Bot, config: PluginConfig):
    # no args, no service
    if not responder.args:
        return

    # do nothing if you're not allowed to post here
    if config.get('do_not_post') and responder.room.room_id in config.get('do_not_post'):
        logger.debug('Room ID is in do_not_post, skipping')
        return

    cache = bot.plugin_cache

    # if --previous or -p, show previous post's link
    if (responder.args[0] == '--previous' or responder.args[0] == '-p'):
        if cache.get('previous_id'):
            await responder.send_text(
                f"https://derpibooru.org/{cache.get('previous_id')}",
                reply=True
            )
        return


    # get default tags for this room
    room_settings = config.get('room_settings')
    default_tags = ''
    if room_settings:
        for setting in room_settings:
            if setting.get('room_id') == responder.room.room_id:
                default_tags = setting.get('default_tags')
                break

    # no poop filter
    search_api = API + '/search/images?per_page=50&filter_id=151346&q='
    filesize_limit = config.get('filesize_limit') * 1000000 if config.get('filesize_limit') else None

    # !derpi oc only, cheese, huge cock, etc
    tags = ' '.join(responder.args)
    if default_tags:
        tags += ', ' + default_tags

    # get a random image and save to disk
    img_path = await _get_random_image(search_api + tags, cache, filesize_limit)

    if not img_path:
        await responder.send_text("No results found!", reply=True)
        return

    await responder.send_file(img_path, reply=True)

    try:
        logger.debug(f"Deleting temp file {img_path}")
        os.remove(img_path)
    except:
        logger.warn(f"Failed to delete temp file {img_path}")



async def _get_random_image(search_api: str, cache: dict, limit: Optional[int]) -> Union[str, None]:
    '''Get a random image url from derpi api
    '''
    # get a list of image posts
    images = await _search(search_api)

    # nothing found: stop
    if not images:
        return

    # get a random image dict from images
    img_dict = random.choice(images)

    # get a list of img urls
    img_urls = img_dict.get('representations')
    if not img_urls:
        return

    # download the image, but try not to exceed the filesize
    sizes = ['full', 'large', 'medium', 'small']
    for key in sizes:
        # URL missing for some reason? stop
        url = img_urls.get(key)
        if not url:
            return
        img_bytes = await _get_image_bytes(url)
        # couldn't get bytes from request? stop
        if not img_bytes:
            logger.debug("failed to get image bytes")
            return
        # well, you're stuck with this one now
        if not limit or len(img_bytes) <= limit or key == 'small':
            ext = splitext(url)[-1]
            temp_filename: str = f"{uuid.uuid4().hex}{ext}"
            await _save(img_bytes, temp_filename)
            cache['previous_id'] = img_dict.get('id')
            return temp_filename
    return


async def _get_image_bytes(img_url: str) -> Union[bytes, None]:
    logger.debug(f"getting {img_url}")
    try:
        r = requests.get(img_url)
        if r.status_code != 200:
            logger.debug("status code not 200")
            return
        logger.debug('returning content')
        return r.content
    except:
        logger.debug('failed to get image {img_url}')
        return


async def _search(search_api: str) -> Union[list[dict], None]:
    '''Query Derpibooru's search API and return the result as a list of dicts
    '''
    r = requests.get(search_api)
    if r.status_code != 200:
        return

    data = json.loads(r.text)
    images: list[dict] = data.get('images')
    return images


async def _save(data: bytes, filename: str):
    async with aiofiles.open(filename, 'wb') as f:
        await f.write(data)
    return
