from core.plugintools import command, Bot, Responder

ADMIN = 100
MOD = 50

plugin_description = "Administrative tool for loading/unloading plugins and other stuff"

@command('unload', params='<plugin name>')
async def unload(responder: Responder, bot: Bot):
    args = responder.args

    # no args, no service
    if not args:
        await responder.send_text(f"Please specify a plugin name", reply=True)
        return

    if responder.sender_power_level() < MOD:
        await responder.send_text(f"Only moderators can run this command!", reply=True)
        return

    plugin_name = args[0]
    plugin_manager = bot.plugin_manager

    success = plugin_manager.unload_plugin(plugin_name)
    if not success:
        await responder.send_text(f"Plugin '{plugin_name}' not found!", reply=True)
    else:
        await responder.send_text(f"Plugin '{plugin_name}' unloaded.", reply=True)


@command('reload', params='<plugin name>')
async def reload(responder: Responder, bot: Bot):
    args = responder.args

    # no args, no service
    if not args:
        await responder.send_text(f"Please specify a plugin name", reply=True)
        return

    if responder.sender_power_level() < MOD:
        await responder.send_text(f"Only moderators can run this command!", reply=True)
        return

    plugin_name = args[0]
    plugin_manager = bot.plugin_manager

    success = plugin_manager.reload_plugin(plugin_name)
    if not success:
        await responder.send_text(f"Plugin '{plugin_name}' not found!", reply=True)
    else:
        await responder.send_text(f"Plugin '{plugin_name}' reloaded.", reply=True)


@command('load', params='<plugin name>')
async def load(responder: Responder, bot: Bot):
    args = responder.args

    # no args, no service
    if not args:
        await responder.send_text(f"Please specify a plugin name", reply=True)
        return

    if responder.sender_power_level() < ADMIN:
        await responder.send_text(f"Only admins can run this command!", reply=True)
        return

    plugin_name = args[0]
    plugin_manager = bot.plugin_manager

    success = plugin_manager.load_plugin(plugin_name)
    if not success:
        await responder.send_text(f"Plugin '{plugin_name}' failed to load! Does it exist? Is it loaded already?", reply=True)
    else:
        await responder.send_text(f"Plugin '{plugin_name}' loaded.", reply=True)


@command('quit', params='')
async def quit(responder: Responder, bot: Bot):
    if responder.sender_power_level() < ADMIN:
        await responder.send_text(f"Only admins can run this command!", reply=True)
        return

    await responder.send_text("Okay, if you want me to...", reply=True)
    await bot.quit()
