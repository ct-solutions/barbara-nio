from core.plugintools import command, trigger, timer, Responder, Sender, PluginConfig

plugin_description = "A sample plugin"

@command('cheese', 'c', params='[message to echo]')
async def cheese(responder: Responder, config: PluginConfig):
    my_api_key = config.get('my_api_key')
    message = ' '.join(responder.args)
    await responder.send_text(message, reply=True)
    await responder.send_text(f"my api key is: '{my_api_key}'")


@command('ham', 'h', params='[message to echo]')
async def ham(responder: Responder):
    message = ' '.join(responder.args)
    await responder.send_text(message)


@trigger('based', description="respond to messages containing 'based'")
async def sauce(responder: Responder, sender: Sender):
    await responder.send_text(responder.room.room_id, reply=True)
    sender.set_room_id(responder.room.room_id)
    await sender.send_text('FUCK')
    await responder.react_to_event('Huh?')


@timer(minutes=1, repeat=True, description="say 'blah' every minute")
async def blah(sender: Sender, config: PluginConfig):
    room_id = config.get('send_to_room')
    sender.set_room_id(room_id)
    await sender.send_text('blah every minute')

@timer(minutes=2, repeat=True, description="say 'ohh' every two minutes")
async def ohh(sender: Sender, config: PluginConfig):
    room_id = config.get('send_to_room')
    sender.set_room_id(room_id)
    await sender.send_text('ohh every two minutes')
