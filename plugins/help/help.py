#!/usr/bin/env python3

from core.plugintools import command, Responder, Bot
import logging

plugin_description = "Plugin for displaying help message"

logger = logging.getLogger(__name__)

@command('help', 'h', params='[command | all"] | [trigger name --trigger | timer name --timer]')
async def help(responder: Responder, bot: Bot):
    if len(responder.args) == 0 or responder.args[0] == 'all':
        await show_all(responder, bot)
        return

    args = responder.args
    if args[0] == 'syntax':
        await responder.send_text(
            _syntax(),
            reply=True
        )
        return

    # show detailed help for a trigger
    if '--trigger' in args:
        args.remove('--trigger')
        trigger_hooks = bot.plugin_manager.get_trigger_hooks(args[0])

        # don't bother if no triggers are found
        if not trigger_hooks:
            return

        result = [f"Actions for trigger '{args[0]}':\n"]
        for hook in trigger_hooks:
            result.append(f"- {hook.description}")

        await responder.send_text('\n'.join(result), reply=True)
        return
    # show detailed help fro a timer
    elif '--timer' in args:
        args.remove('--timer')
        timer_hooks = bot.plugin_manager.get_timer_hooks(args[0])

        # don't bother if no timers are found
        if not timer_hooks:
            return

        result = [f"Triggers matching name '{args[0]}':\n"]
        for hook in timer_hooks:
            result.append(f"- {hook.description}")

            await responder.send_text('\n'.join(result), reply=True)
            return

    # finally, if none of the above, show help for command
    prefix = bot.app_config.command_prefix
    hook = bot.plugin_manager.get_command_hook(args[0])
    if hook:
        text = _command_string(prefix, hook)
        await responder.send_text(text, reply=True)


async def show_all(responder: Responder, bot: Bot):
    message = ["Type `!help syntax` for syntax help"]
    plugin_manager = bot.plugin_manager
    command_prefix = bot.app_config.command_prefix

    for plugin in plugin_manager.get_all_plugins():
        message.append(_plugin_string(plugin))
        for command_hook in plugin.get_hooks('command'):
            message.append(_command_string(command_prefix, command_hook))
        for trigger_hook in plugin.get_hooks('trigger'):
            message.append(_trigger_string(trigger_hook))
        timer_hooks = plugin.get_hooks('timer')
        if timer_hooks:
            message.append(_timer_string(timer_hooks))

    text = '\n'.join(message)
    await responder.send_text(text, reply=True)


def _syntax() -> str:
    result = []
    result.append("Syntax help:\n")
    result.append("- `<argument>`: argument is mandatory")
    result.append("- `[argument]`: argument is optional")
    result.append("- `[detailed description of argument]`: single argument explained in multiple words")
    result.append('- `<arg desc | "asdf">`: must provide an argument described by `arg desc` OR write `asdf` literally')
    result.append('- `[arg desc | "asdf"]`: optionally provide an argument described by `arg desc` OR write `asdf` literally')
    return '\n'.join(result)


def _plugin_string(plugin):
    # **plugin**: plugin description \n
    return f"\n**{plugin.name}**: {plugin.description}\n"


def _command_string(cmd_prefix, hook, listitem=True):
    # `!cmd` `arg1` `arg2`
    text = f"`{cmd_prefix}{hook.commands[0]} {hook.params}`"
    if listitem:
        return "- " + text
    return text



def _trigger_string(hook, listitem=True):
    # Triggers: *trigger1, trigger2*, ...
    triggers = ', '.join(hook.triggers)
    text = f'Triggers: *{triggers}*'
    if listitem:
        return "- " + text
    return text


def _timer_string(hooks, listitem=True):
    timer_names = [hook.func.__name__ for hook in hooks]
    timers = ', '.join(timer_names)
    text = f'Timers: *{timers}*'
    if listitem:
        return '- ' + text
    return text
