#!/usr/bin/env python3
from __future__ import annotations

import requests
import json
import logging
from datetime import datetime, timedelta

from typing import List, Dict

from core.plugintools import command, timer, Responder, PluginConfig, Bot, Sender

plugin_description = "Thread status tracker for 4chan"

api_url = "https://a.4cdn.org"
logger = logging.getLogger(__name__)

CATALOGS = 'catalogs'
WATCHED = 'watched'
TIMESTAMP = 'timestamp'

'''
cache layout:
{
    CATALOGS: {'mlp': Catalog, 'e': Catalog, ... },
    WATCHED: [Thread, Thread, ...],
    TIMESTAMP: datetime obj
}
'''

@command('thread', 't', params='<thread name | "all">')
async def thread(responder: Responder, bot: Bot, config: PluginConfig):
    args = responder.args
    cache = bot.plugin_cache

    # don't bother if the user just sends !thread
    if not args:
        await responder.send_text(
            'Please specify a thread name (use "all" to show all watched threads)',
            reply=True
        )
        return

    catalogs, watched_threads = await _get_4chan_data(cache, config)

    # inefficient, but do it this way for now
    threadname = ' '.join(args).lower()
    if threadname == 'all':
        await _print_threads(responder, watched_threads)
    elif threadname in watched_threads:
        await responder.send_text(
            _get_thread_info(watched_threads[threadname]),
            reply=True
        )
    else:
        # exact match not found, do fuzzy matching
        for key in watched_threads:
            if threadname.lower() in key.lower():
                await responder.send_text(
                    _get_thread_info(watched_threads[key]),
                    reply=True
                )



@command('search', 's', params='<board name> <thread name>')
async def search(responder: Responder):
    # fuck it, don't cache this result
    args = responder.args

    # do nothing if not enough args
    if not args or len(args) < 2:
        return

    board = args[0]

    catalog = Catalog(board)
    if not catalog.valid:
        await responder.send_text(f"Invalid board name: '{board}'.", reply=True)
        return

    threads = catalog.get_threads_approx(' '.join(args[1:]))
    if not threads:
        await responder.send_text("Sorry, I didn't find any threads containing that phrase.", reply=True)
        return
    logger.debug(f"found threads: {threads}")
    text = _get_thread_info(threads)
    await responder.send_text(text, reply=True)



@timer(minutes=5, repeat=True, description="Check if any of the watched threads are at bump limit or at page 10")
async def check_watched_threads(sender: Sender, bot: Bot, config: PluginConfig):
    cache = bot.plugin_cache
    catalogs, watched_threads = await _get_4chan_data(cache, config)

    bump_warn = []
    page10_warn = []
    for thread_name in watched_threads:
        if not watched_threads.get(thread_name):
            continue
        threads = watched_threads.get(thread_name)
        if not threads:
            return

        # is it at bump limit?
        key_bump = thread_name + 'bump warned'
        if threads[0].bump_limit and not cache.get(key_bump):
            bump_warn.append(_get_thread_info(threads, bullets=True))
            cache[key_bump] = True
        # if not, is there a leftover flag set from the previous thread?
        elif not threads[0].bump_limit and cache.get(key_bump):
            cache[key_bump] = False

        # is it at page 10?
        key_p10 = thread_name + 'page10 warned'
        if threads[0].page_10 and not cache.get(key_p10):
            page10_warn.append(_get_thread_info(threads, bullets=True))
            cache[key_p10] = True
        # if not, is there a leftover flag set from when it was at page 10?
        elif not threads[0].page_10 and cache.get(key_p10):
            cache[key_p10] = False

    rooms = config.get('warn_to')
    for room in rooms:
        sender.set_room_id(room)
        newlines = '\n'
        if bump_warn:
            await sender.send_text(
                f"**Thread(s) at bump limit:**\n\n"
                f"{newlines.join(bump_warn)}",
                notice=False
            )
        if page10_warn:
            await sender.send_text(
                f"**Thread(s) at page 10:**\n\n"
                f"{newlines.join(page10_warn)}",
                notice=False
            )


async def _get_4chan_data(
        cache: dict,
        config: PluginConfig
) -> tuple[dict[str, Catalog], dict[str, list[Thread]]]:
    '''Fetch JSON from 4chan API if the data stored in cache is stale (older than 1 min.).
    Updates the plugin cache with new values if a new GET request is made.
    Args:
        cache: Plugin cache
        config: Plugin config
    Returns:
        catalogs: {'board name': Catalog obj, ... }
            Contains catalog objects from boards whose threads are watched in
            the plugin's config file
        watched_threads: { 'thread title': [Thread, Thread, ...], ... }
            Contains thread objects of watched threads.
            Assume that the first element of the [Thread, Thread,..] list is the
            most recently bumped thread.
    '''

    now = datetime.now()
    # keep it fresh every 3 mins
    delta = timedelta(minutes=3)

    if cache.get(CATALOGS) and now - cache.get(TIMESTAMP) < delta:
        logger.debug("Fetching existing data from cache")
        return cache.get(CATALOGS), cache.get(WATCHED)

    thread_configs = _get_thread_configs(config)
    catalogs: dict[str, Catalog] = {}
    watched_threads: dict[str, list[Thread]] = {}

    for board in thread_configs:
        c = Catalog(board)
        if not c.valid:
            logger.warn(f"Board {board} not found")
            continue
        catalogs[board] = Catalog(board)
        for thread in thread_configs[board]:
            title = thread['title']
            watched_threads[title] = catalogs[board].get_threads(title, thread['keywords'])

    cache.update({
        CATALOGS: catalogs,
        WATCHED: watched_threads,
        TIMESTAMP: now
    })
    return catalogs, watched_threads


async def _print_threads(responder: Responder, watched_threads):
    '''Print the status of all watched threads
    '''
    result = []
    for name in watched_threads:
        threads = watched_threads[name]
        result.append(_get_thread_info(threads, name=name))
    msg = '\n\n'.join(result)
    await responder.send_text(msg, reply=True)


def _get_thread_info(threads: List[Thread], name='', bullets=False) -> str:
    if len(threads) == 0:
        return f"{name}: Not found"

    # just assume that the one closest to page 1 is the valid one
    thread = threads[0]

    title = (thread.title[:47] + '...') if len(thread.title) > 50 else thread.title
    page_num = f"📄 {thread.page_num}"
    replies = f"🗨️ {thread.raw_dict.get('replies')}"

    result = f"[{title}]({thread.url}) {page_num} {replies}"
    return "- " + result if bullets else result


def _get_thread_configs(config) -> Dict[str, List[Dict]]:
    '''Returns:
        {
            'mlp': [
                    {
                        'name': '..',
                        'title': '...',
                        'board': '...',
                        'keywords': ['...', ]
                    },
            ],
            'a': [dict(), dict(), ...],
            ...
        }
    '''
    board_names = [thread['board'] for thread in config.get('threads')]
    thread_configs: Dict[str, List] = dict([(board, []) for board in board_names])

    for thread in config.get('threads'):
        board_name = thread['board']
        thread_configs[board_name].append(thread)
    return thread_configs


class Thread:
    def __init__(self, data: dict, board: str, page_num: int):
        '''Encapsulates a 4chan thread
        Args:
            data: JSON data
            board: name of the board
            page_num: where the thread is on the board
        '''
        self.raw_dict = data

        self.board = board
        self.page_num = page_num
        self.title = data['sub']
        self.number = data['no']
        self.body = data['com']
        self.timestamp = data['time']
        self.url = f"https://boards.4channel.org/{self.board}/thread/{self.number}"
        self.bump_limit = data['bumplimit'] == 1
        self.page_10 = page_num == 10



class Catalog:
    def __init__(self, board: str):
        '''Encapsulates a 4chan board catalog
        Args:
            board: name of the board
        '''
        self.__pages: List[Dict]

        # name of the board
        self.board: str = board

        # True if catalog loaded successfully, false outherwise
        self.valid: bool

        self.__load_catalog_data()


    def get_threads(self, title: str, keywords: List[str]) -> List[Thread]:
        '''Get all threads that contain a certain title and keywords
        Args:
            title: Title of the thread (case insensitive)
            keywords: Words/phrases to look for in the OP body (case insensitive)
        Returns:
            A list of Thread objects that match the criteria
        '''
        result = []
        for page in self.__pages:
            threads = page['threads']
            for thread in threads:
                # some threads have neither, what the fuck
                if 'sub' not in thread or 'com' not in thread:
                    continue
                if title.lower() in thread['sub'].lower():
                    for keyword in keywords:
                        if keyword.lower() in thread['com']:
                            result.append(
                                Thread(thread, self.board, int(page['page']))
                            )
        return result


    def get_threads_approx(self, phrase: str) -> list[Thread]:
        '''Get all threads that contain a phrase in either title or body
        Args:
            words: list of words to match
        '''
        result = []
        for page in self.__pages:
            threads = page['threads']
            for thread in threads:
                if 'sub' not in thread or 'com' not in thread:
                    continue
                if phrase in thread['sub'].lower() or phrase in thread['com']:
                    result.append(
                        Thread(thread, self.board, int(page['page']))
                    )
        return result



    def __load_catalog_data(self):
        '''Fetch catalog data from 4chan API
        '''
        url = f"{api_url}/{self.board}/catalog.json"

        try:
            response = requests.get(url)
            self.__pages = json.loads(response.text)
            self.valid = True
            logger.debug(f"Data fetched successfully for board {self.board}: {len(self.__pages)} pages")
        except:
            logger.warn("Failed to get data from 4chan API")
            self.valid = False
