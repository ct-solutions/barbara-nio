# Plugins 

## Folder Structure 

To create a `MyPlugin` plugin, add a folder under `plugins/` and populate it with `MyPlugin.py`: 

```text 
plugins/
└── MyPlugin/
    └── MyPlugin.py
```

Optionally, if you want your plugin to have its own configuration file, add a `config.yaml` file in the same folder: 

```text
plugins/
└── MyPlugin/
    ├── config.yaml
    └── MyPlugin.py
```


An example plugin can be found under [plugins/example_plugin](./plugins/example_plugin). 

## Arguments 

Barbara-nio uses function signature inspection to determine which arguments to use for invoking plugin functions. 

When writing plugins, import what you need from `core.plugintools` and use Python's type hinting to declare what you want from the bot. 

Example: 

``` python
from core.plugintools import command, Responder

@command('box', 'b', params='<width> <height>')
async def print_box(responder: Responder): 
    width = responder.args[0] 
    height = responder.args[1] 
    await responder.send_text(f"Your box is {width} by {height} inches!")
```

### core.plugintools 

Everything you'll need for writing plugins is contained within `core.plugintools`: 

- Objects for sending messages to rooms:
  - Responder 
  - Sender 
- Objects for accessing plugin settings or other bot stuff:
  - Bot
  - PluginConfig 
- Decorators for defining functions to be invoked by the bot: 
  - command 
  - trigger 
  - timer

The rest of this README describes each component in detail. 

## Responding to User Input or Timed Events 

Barbara-nio has three plugin-specific decorators -- command, trigger, and timer: 

- `@command`: something invoked actively by users, such as `!help` 
- `@trigger`: something triggered by users saying a specific keyword
- `@timer`: something invoked periodically by the bot without any user input 


### Command 

``` python
@command(*command_names, params: str)
```

- `*command_names`: arbitrary number of comma-separated aliases to give to this command. 
- `params`: helpstring for the `!help` command. You can honestly write whatever you want here as long as it's descriptive. 

Example: 

``` python
from core.plugintools import Responder, command 

@command('help', 'h', params='<command name>')
async def show_help(responder: Responder): 
    await responder.send_text("this is a helpful message", reply=True)
```

### Trigger 

``` python
@trigger(*triggers, description: str) 
```

- `*triggers`: arbitrary number of comma-separated trigger words to respond to.
- `description`: helpstring for the `!help` command. Describe what the trigger does here. 

Example: 

``` python
from core.plugintools import Responder, trigger

@trigger('sauce?', 'source?', description="Respond appropriately to anyone asking for sauce") 
async def give_sauce(responder: Responder): 
    await responder.send_text('This occurred to me in a dream', reply=True) 
```

### Timer 

``` python
@timer(description: str, hours: int = 0, minutes: int = 0, repeat: bool = False)
```

- `description`: a brief description of what this timer is about 
- `hours`: run this once every this many hours 
- `minutes`: run this once every this many minutes 
- `repeat`: whether to keep running this timer or not (not used at the moment, sorry!) 

Example: 

``` python
from core.plugintools import Sender, timer 

@timer(description='Say something funny every 1 hour 30 minutes', hours=1, minutes=30, repeat=True) 
async def funny(sender: Sender):
    # Set which room to send messages to. 
    # Hardcoded here for brevity.
    sender.set_room_id('room-id-string:matrix.org')
    
    await sender.send_text('This is something funny')
```

## Sending Messages to Rooms 

Barbara-nio has two ways to send messages to rooms: 

- `Responder`: contains all the necessary stuff for responding to messages. Mostly useful for commands and triggers. 
- `Sender`: contains all the necessary stuff for sending message to arbitrary rooms. 

### Responder 

Use this object if you're responding to an event or a message. 

Member variables: 
- `args`: text sent by the user, excluding the command portion. For example, if the user typed `!MyCommand arg1 arg2 arg3`, then `args` would contain `['arg1', 'arg2', 'arg3']`.
- `event`: reference to the nio `Event` object (or one of its inherited objects) that triggered this invocation. Usually, this is a [`RoomMessageText`](https://matrix-nio.readthedocs.io/en/latest/nio.html#nio.events.room_events.RoomMessageText) instance, unless you're responding to another event. 
- `room`: reference to the nio [`MatrixRoom`](https://matrix-nio.readthedocs.io/en/latest/nio.html#nio.rooms.MatrixRoom) object where this event was triggered. 

Functions: 
- `send_text(text: str, reply=False, notice=True)`: send a message to the room that triggered this invocation. If `reply=True`, message is sent as a reply to the message. If `notice=True`, message is sent as a [`m.notice`](https://spec.matrix.org/v1.2/client-server-api/#mnotice) message type. 
- `send_file(filepath: str, reply=False)`: send a file to the room that triggered this invocation. 
- `sender_power_level()`: returns the sender's [power level](https://matrix.org/docs/guides/moderation#power-levels) as integer. 
- `react_to_event(reaction: str)`: react to the event that triggered this invocation. `reaction` can be plaintext or an emoji. 

Example: 

``` python
from core.plugintools import Responder, command 

# Respond to !give_cheese or `!give`
@command('give_cheese', 'give', params='<cheese>')
async def send_cheese(responder: Responder): 
    # If the user typed '!give_cheese shredded pepperjack', then sender_cheese = 'shredded pepperjack'
    sender_cheese = ' '.join(responder.args) 
    
    # Room ID string of where the user typed the command 
    room_id = responder.room.room_id 
    
    # Timestamp of when the server received user's message 
    msg_timestamp = responder.event.server_timestamp

    text = f"You sent me {sender_cheese} from {room_id} at {msg_timestamp}... "
    if 'cheddar' not in sender_cheese: 
        text += "But I wanted cheddar!" 
    
    # Send it as a non-reply notice type 
    await responder.send_text(text, reply=False) 
    
    # Send cheese.jpeg while you're at it 
    await responder.send_file('cheese.jpg', reply=True)
```

### Sender 

Use this object if you're trying to send messages to a room unprompted. Mostly useful for functions that use the `@timer` decorator, but you can use it for other purposes as well. 

Functions: 
- `set_room_id(room_id: str)`: set which room to send messages to. Since you're sending messages unprompted, you'll have to call this manually to determine *where* to send messages to.
- `send_text(text: str, notice=True)`: send text to the room configured by `set_room_id()` function. Similar to `Responder`'s function, minus the reply bit. 
- `send_file(filepath: str)`: send file to room configured by `set_room_id()` function. Similar to `Responder`'s function, minus the reply bit. 

Example: 

``` python
from core.plugintools import Responder, Sender, trigger 

@trigger('based', description='Rat someone out whenever they say "based"')
async def not_based(responder: Responder, sender: Sender): 
    offender_room_id = responder.room.room_id 
    sender.set_room_id('room-id-for-admin-dm:matrix.org')
    
    await responder.send_text(
        'You said the b-word! I am telling admin on you..', 
        reply=True
    ) 
    await sender.send_text(
        f'Hey boss, someone said "based" in room {offender_room_id}. '
        'You should go yell at them!'
    )
```

## Doing Advanced Stuff 

Barbara-nio provides objects for doing administrative stuff or interacting with plugin configurations: 
- `Bot`: contains stuff for doing administrative tasks or interacting with the plugin cache. 
- `PluginConfig`: a [PluginConfig](../core/config.py) instance that contains stuff for interacting with plugin-specific configurations. 

### Bot 

Member variables: 
- `client`: an [AsyncClient](https://matrix-nio.readthedocs.io/en/latest/nio.html#asyncclient) instance for doing stuff directly to the client. 
- `store`: a [Storage](../core/storage.py) instance for interacting with SQLite backend. 
- `plugin_cache`: a Python `dict()` allocated specifically for this plugin. Persistent as long as the bot is up.
- `app_config`: an [AppConfig](../core/config.py) instance for interacting with global bot configurations. 
- `plugin_manager`: a [PluginManager](../core/plugin_manager.py) instance for interacting with the plugin manager. 

Functions: 
- `quit()` disconnect and shut down. 

Example: 

``` python
from core.plugintools import Responder, Bot, command 

@command('hello', params='[any message]')
async def hello(responder: Responder, bot: Bot): 
    plugin_cache: dict = bot.plugin_cache 
    sender_id: str = responder.room.sender
    
    # Say hello if nobody said hello before 
    if not plugin_cache.get('sender'): 
        await responder.send_text('Hello!', reply=True) 
        plugin_cache['sender'] = sender_id 
    else: 
        await responder.send_text(
            f"Thanks, but user {plugin_cache.get('sender')} already greeted me",
            reply=True
        ) 
```

### PluginConfig 

Member variables: 
- `config_dict`: a Python `dict` containing all key-value pairs of the configuration. 

Functions: 
- `get(key: str)`: get plugin config option defined by key. 

Example: 

``` yaml
# plugins/MyPlugin/config.yaml 
 my_api_key: 'abc-def-123'
```

``` python
# plugins/MyPlugin/MyPlugin.py 

import requests 
import aiofiles
from core.plugintools import Sender, PluginConfig, timer 

@timer(hours=1, description="Download file from example.com every hour")
async def downloader(sender: Sender, plugin_config: PluginConfig): 
    # Send any messages to your DM 
    sender.set_room_id('your-dm-room-id:matrix.org') 
    
    # Get API key from your plugin config and set the API URL 
    api_key = plugin_config.get('my_api_key')
    api_url = f'https://example.com/api/key={api_key}&search?='
    
    # Send the request and handle any failures 
    r = requests.get(f"{api_url}cheese grater")
    if not r or r.status_code != 200: 
        await sender.send_text("Failed to get anything from example.com.")
        return 
        
    # Save it as file 
    data = r.content 
    try:
        async with aiofiles.open('output.txt', 'w') as f: 
            await f.write(data) 
    except: 
        await sender.send_text("Failed to save API query output!")
```
