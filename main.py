#!/usr/bin/env python3
import asyncio
import logging
import sys
from time import sleep

from apscheduler.triggers.interval import IntervalTrigger
from apscheduler.schedulers.asyncio import AsyncIOScheduler

from aiohttp import ClientConnectionError, ServerDisconnectedError
from nio import (
    AsyncClient,
    AsyncClientConfig,
    InviteMemberEvent,
    LocalProtocolError,
    LoginError,
    MegolmEvent,
    RoomMessageText,
    UnknownEvent,
)

from core.callbacks import Callbacks
from core.config import AppConfig
from core.storage import Storage
from core.plugin_manager import PluginManager

logger = logging.getLogger(__name__)
scheduler = AsyncIOScheduler()

class App:
    def __init__(self, config_path: str):
        self.__app_config = AppConfig(config_path)
        self.__store = Storage(self.__app_config.database)
        self.__plugin_manager = PluginManager(self.__app_config)

        self.__client: AsyncClient
        self.__callbacks: Callbacks

        self.__setup_client()
        self.__setup_callbacks()


    async def poll_timers(self):
        logger.debug('Polling timers')
        await self.__callbacks.timer()


    async def connect_forever(self):
        config = self.__app_config
        client = self.__client
        while True:
            await asyncio.sleep(0)
            try:
                if config.user_token:
                    # Use token to log in
                    client.load_store()

                    # Sync encryption keys with the server
                    if client.should_upload_keys:
                        await client.keys_upload()
                else:
                    # Try to login with the configured username/password
                    try:
                        logger.debug("Trying to log in...")
                        login_response = await client.login(
                            password=config.user_password,
                            device_name=config.device_name,
                        )

                        # Check if login failed
                        if type(login_response) == LoginError:
                            logger.error("Failed to login: %s", login_response.message) #type:ignore
                            return False
                    except LocalProtocolError as e:
                        # There's an edge case here where the user hasn't installed the correct C
                        # dependencies. In that case, a LocalProtocolError is raised on login.
                        logger.fatal(
                            "Failed to login. Have you installed the correct dependencies? "
                            "https://github.com/poljar/matrix-nio#installation "
                            "Error: %s",
                            e,
                        )
                        return False
                    except Exception as e:
                        logger.fatal(f"Something fucked up: {type(e)} - {e}")
                        return False

                    # Login succeeded!

                logger.info(f"Logged in as {config.user_id}")
                await client.sync_forever(timeout=30000, full_state=True)

            except (ClientConnectionError, ServerDisconnectedError):
                logger.warning("Unable to connect to homeserver, retrying in 15s...")

                # Sleep so we don't bombard the server with login requests
                await asyncio.sleep(0)
                sleep(15)
            finally:
                # Make sure to close the client connection on disconnect
                await client.close()
                logger.info('Client closed')
                #return


    def __setup_callbacks(self):
        client = self.__client
        callbacks = Callbacks(
            self.__client, self.__store, self.__app_config, self.__plugin_manager
        )

        client.add_event_callback(callbacks.message, (RoomMessageText,)) #type:ignore
        client.add_event_callback(
            callbacks.invite_event_filtered_callback, (InviteMemberEvent,) #type:ignore
        )
        client.add_event_callback(callbacks.decryption_failure, (MegolmEvent,)) #type:ignore
        client.add_event_callback(callbacks.unknown, (UnknownEvent,)) #type:ignore

        self.__callbacks = callbacks


    def __setup_client(self):
        client_config = AsyncClientConfig(
            max_limit_exceeded=0,
            max_timeouts=0,
            store_sync_tokens=True,
            encryption_enabled=True,
        )

        # Initialize the matrix client
        client = AsyncClient(
            self.__app_config.homeserver_url,
            self.__app_config.user_id,
            device_id=self.__app_config.device_id,
            store_path=self.__app_config.store_path,
            config=client_config,
        )

        if self.__app_config.user_token:
            client.access_token = self.__app_config.user_token
            client.user_id = self.__app_config.user_id

        self.__client = client



async def main():
    if len(sys.argv) > 1:
        config_path = sys.argv[1]
    else:
        config_path = "config.yaml"

    app = App(config_path)

    # fuck it, just poll all of them once per minute
    scheduler.start()
    scheduler.add_job(app.poll_timers, trigger=IntervalTrigger(minutes=1))

    await app.connect_forever()
